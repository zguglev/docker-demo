package com.primeholding.internship.docker.demo.controller;

import com.jhlabs.image.GaussianFilter;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.validation.constraints.DecimalMin;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@RestController
@RequestMapping("/api/v1/image")
public class ImageEditor {
    private static final Logger logger = LoggerFactory.getLogger(ImageEditor.class);

    @ApiOperation(value = "Executes gaussian blur over image in PNG format and returns the new result")
    @PostMapping(value = "/blur/{radius}", consumes = "multipart/form-data", produces = "image/png")
    public ResponseEntity<byte[]> blurImage(
            @PathVariable("radius") @DecimalMin(value = "0.1", message = "Blur radius must be at least 0.1") float radius,
            @RequestBody MultipartFile file) throws IOException {

        String resultFileName = generateResultFileName(radius, file.getOriginalFilename());
        ByteArrayOutputStream result = blurImage(radius, file, resultFileName);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=\"%s\"", resultFileName))
                .body(result.toByteArray());
    }

    private ByteArrayOutputStream blurImage(float radius, MultipartFile file, String resultFileName) throws IOException {
        BufferedImage origImg = ImageIO.read(file.getInputStream());

        logger.info("Blurring {} with brush radius of {}...", resultFileName, radius);
        BufferedImage blurredImg = new GaussianFilter(radius).filter(origImg, null);
        logger.info("Done blurring of {}", resultFileName);

        ByteArrayOutputStream result = new ByteArrayOutputStream((int) (file.getSize() + (file.getSize() * 0.02)));
        ImageIO.write(blurredImg, "png", result);
        return result;
    }

    private String generateResultFileName(float radius, String origFileName) {
        int pngExtensionLocation = origFileName.toLowerCase().lastIndexOf(".png");
        if (pngExtensionLocation == -1) pngExtensionLocation = origFileName.length() - 1;
        return origFileName.substring(0,  pngExtensionLocation).concat(String.format("-%.2f-blurred.png", radius));
    }
}
