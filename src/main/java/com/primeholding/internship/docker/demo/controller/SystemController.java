package com.primeholding.internship.docker.demo.controller;

import org.apache.tomcat.jni.Time;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/api/v1/system")
public class SystemController {
    private ApplicationContext applicationContext;

    public SystemController(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @GetMapping("/die")
    public void die() {
        Executors.newSingleThreadScheduledExecutor()
                .schedule(
                        () -> {
                            int exitCode = SpringApplication.exit(applicationContext, () -> 55);
                            System.exit(exitCode);
                        },
                        2000,
                        TimeUnit.MILLISECONDS
                );
    }
}
