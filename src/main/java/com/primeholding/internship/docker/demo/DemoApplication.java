package com.primeholding.internship.docker.demo;

import com.primeholding.internship.docker.demo.controller.ImageEditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Bean
    Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                    .apis(RequestHandlerSelectors.basePackage(ImageEditor.class.getPackageName()))
                    .paths(PathSelectors.any())
                    .build();
    }

    @Bean
    CommandLineRunner envPrinterRunner() {
        return (String... args) -> {
            Logger logger = LoggerFactory.getLogger(this.getClass());
            logger.info("Environment variables:\n{}", System.getenv());
            logger.info("CLI args:\n{}", (Object) args);
        };
    }

}
